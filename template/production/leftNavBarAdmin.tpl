<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="index.php" class="site_title"><i class="fa fa-graduation-cap"></i> <span>Extranet CCI </span></a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile clearfix">
            <div class="profile_info">
                <span>Bienvenue</span>
                <h2>Nom Élève</h2>
            </div>
        </div>
        <!-- /menu profile quick info -->

        <br />

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                    <li>
                        <a href="index.php"><i class="fa fa-home"></i>Accueil</a>
                    </li>
                    <li>
                        <a><i class="fa fa-users"></i>Profils <span class="fa fa-chevron-down"></a>
                        <ul class="nav child_menu">
                            <li><a href=".html">Listes des Profils</a></li>
                            <li><a href=".html">Ajouter un profil</a></li>
                        </ul>
                    </li>
                    <li>
                        <a><i class="fa fa-trophy"></i> Compétences <span class="fa fa-chevron-down"></a>
                        <ul class="nav child_menu">
                            <li><a href=".html">Listes des compétences</a></li>
                            <li><a href=".html">Ajouter une compétence</a></li>
                        </ul>
                    </li>
                    <li>
                        <a><i class="fa fa-book"></i> Formations <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href=".html">Listes des Formations</a></li>
                            <li><a href=".html">Ajouter une Formation</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>
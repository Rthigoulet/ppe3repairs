<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="index.php" class="site_title"><i class="fa fa-graduation-cap"></i> <span>Extranet CCI </span></a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile clearfix">
            <div class="profile_info">
                <span>Bienvenue</span>
                <h2>{$nomAffiche}</h2>
            </div>
        </div>
        <!-- /menu profile quick info -->

        <br />

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                    <li>
                        <a href="index.php"><i class="fa fa-home"></i>Accueil</a>
                    </li>
                    <li>
                        <a href=".php"><i class="fa fa-user"></i> Profil</a>
                    </li>
                    <li>
                        <a><i class="fa fa-trophy"></i> Compétences <span class="fa fa-chevron-down"></a>
                        <ul class="nav child_menu">
                            <li><a href="index.php?gestion=comp">Tableau de bord</a></li>
                            <li><a href=".html">Valider une compétence</a></li>
                        </ul>
                    </li>
                    <li>
                        <a><i class="fa fa-book"></i> Situations <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="index.php?gestion=situation">Listes des situations</a></li>
                            <li><a href="index.php?gestion=situation&action=ficheSituation">Ajouter une situation</a></li>
                        </ul>
                    </li>
                    <li>
                        <a><i class="fa fa-comment"></i> Commentaires </a> 
                    </li>
                    <li>
                        <a><i class="fa fa-envelope"></i>Contact</a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            {*<a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>*}
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            {*<a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>*}
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>
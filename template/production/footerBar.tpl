<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="images/favicon.ico" type="image/ico" />

        <!-- Bootstrap -->
        <link href="template/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awes..ome -->
        <link href="template/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="template/vendors/nprogress/nprogress.css" rel="stylesheet">
        <!-- iCheck -->
        <link href="template/vendors/iCheck/skins/flat/green.css" rel="stylesheet">

        <!-- bootstrap-progressbar -->
        <link href="template/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
        <!-- JQVMap -->
        <link href="template/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
        <!-- bootstrap-daterangepicker -->
        <link href="template/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="template/build/css/custom.min.css" rel="stylesheet">
    </head>


    <!-- footer content -->
    <footer>
        <div class="pull-right" style=''>
            Extranet CCI - Site de gestion de compétence <a href="index.php">Bts Sio 2019</a>
        </div>
        <div class="clearfix"></div>
    </footer>
    <!-- /footer content -->
    


    <!-- jQuery -->
    <script src="template/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="template/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="template/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="template/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="template/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="template/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="template/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="template/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="template/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="template/vendors/Flot/jquery.flot.js"></script>
    <script src="template/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="template/vendors/Flot/jquery.flot.time.js"></script>
    <script src="template/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="template/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="template/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="template/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="template/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="template/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="template/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="template/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="template/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="template/vendors/moment/min/moment.min.js"></script>
    <script src="template/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="template/build/js/custom.min.js"></script>

</body>
</html>
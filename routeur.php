<?php

require_once('mod_' . $gestion . '/controleur/' . $gestion . 'Controleur.php');

if (isset($_REQUEST['action'])) {

    switch ($_REQUEST['action']) {
       
        /* Authentification */
        case 'authentification':
            //L'utilisateur cherche à s'authentifier
            authentification($_REQUEST);
            
            break;
        /* Authentification */

        case 'deconnexion':
            //L'utilisateur demande la deconnexion
            deconnexion();
            break;
        
        case 'ficheSituation':
            vueParDefaut("ficheSituation");;
            break;
        
        default:
            echo "IMPOSSIBLE DE PASSER ICI !!!";
            break;
    }
} else {    
    vueParDefaut();
}
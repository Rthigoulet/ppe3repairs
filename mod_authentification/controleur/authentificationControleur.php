<?php

require 'mod_authentification/modele/authentificationModele.php';

function vueParDefaut($message = null) {


    require_once 'mod_authentification/vue/authentificationVue.php';
}

function deconnexion($message = null) {
    
    // Supression des variables de session et de la session
    $_SESSION = array();
    session_destroy();

    header('Location: index.php');
    exit;
}

function authentification($parametre, $message = null) {

    $idRequete = authentificationExist($parametre);

    if ($idRequete->rowcount() == 1) {
        $row = $idRequete->fetch(PDO::FETCH_NUM);
        $temp = $parametre['motDePasse'];
        /* Cryptage & décryptage */
        $gauche = 'ar30&y%';
        $droite = 'tk!@';
        $jeton = hash('ripemd128', "$gauche$temp$droite");
        /* Cryptage & décryptage */

        //$jeton = $temp;


        if ($jeton == $row[2]) {
            session_start();
            $_SESSION['login'] = $row[1];
            $_SESSION['prenom'] = $row[3];
            $_SESSION['nom'] = $row[4];
            
            $rest = substr($row[8], 0, 1);
            
            switch ($rest){
                case "A":
                    $_SESSION['menu'] = 'template/production/leftNavBarEleve.tpl';
                    $_SESSION['home'] = 'template/production/accueilEleve.tpl';
                    break;
                case "S":
                    $_SESSION['menu'] = 'template/production/leftNavBarAdmin.tpl';
                    $_SESSION['home'] = 'template/production/accueilAdmin.tpl';
                    break;
                case "T":
                    $_SESSION['menu'] = 'template/production/leftNavBarTuteur.tpl';
                    $_SESSION['home'] = 'template/production/accueilTuteur.tpl';
                    break;
                case "F":
                    $_SESSION['menu'] = 'template/production/leftNavBarFormateur.tpl';
                    $_SESSION['home'] = 'template/production/accueilFormateur.tpl';
                    break;
                default:
                    
                    deconnexion("Vous n'êtes pas autorisé à entrer ici");
                    break;
            }
            

            header('location:index.php');
        } else {

            require_once 'mod_authentification/vue/authentificationVue.php';
        }
    } else {

        require_once 'mod_authentification/vue/authentificationVue.php';
    }
}

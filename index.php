<?php

session_start();

$root = dirname(__FILE__) . DIRECTORY_SEPARATOR;

set_include_path('.' .
        PATH_SEPARATOR . $root . 'include' . DIRECTORY_SEPARATOR .
        PATH_SEPARATOR . $root . 'template' . DIRECTORY_SEPARATOR .
        PATH_SEPARATOR . get_include_path()
);


require_once('configuration.php');




//Obligatoire pour l'authentification
/* Authentification */
if(!isset($_SESSION['login'])){
    
    $gestion = 'authentification';
    
}else if (isset($_REQUEST['gestion'])) {

    $gestion = $_REQUEST['gestion'];  

   
    
} else {
    
    $gestion = 'accueil'; //Ce sera accueil ensuite !!
    
}
/* Authentification */

require_once('routeur.php');
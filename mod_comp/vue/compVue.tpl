<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="images/favicon.ico" type="image/ico" />

        <title>{$titreOnglet}</title>

        <!-- Bootstrap -->
        <link href="template/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awes..ome -->
        <link href="template/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="template/vendors/nprogress/nprogress.css" rel="stylesheet">
        <!-- iCheck -->
        <link href="template/vendors/iCheck/skins/flat/green.css" rel="stylesheet">

        <!-- bootstrap-progressbar -->
        <link href="template/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
        <!-- JQVMap -->
        <link href="template/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
        <!-- bootstrap-daterangepicker -->
        <link href="template/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="template/build/css/custom.min.css" rel="stylesheet">
    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">

                {include $leftNavBar} {*Appel Menu de gauche *}
                {include 'template/production/topNavBar.tpl'}  {*Appel Menu du Haut*}


                <!-- page content -->
                <div class="right_col" role="main">
                    <!-- top tiles -->
                    <center>
                        <h3>Tableau de bord des Compétences</h3>
                        <br>
                    </center>
                    <!-- /top tiles -->

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="dashboard_graph">

                                <div class="row">

                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="x_panel tile fixed_height_320 overflow_hidden">
                                            <div class="x_title">
                                                <h2>Compétences Obligatoires</h2>
                                                <ul class="nav navbar-right panel_toolbox">
                                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                    </li>
                                                    <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li><a href="#">Settings 1</a>
                                                            </li>
                                                            <li><a href="#">Settings 2</a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                                    </li>
                                                </ul>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="x_content">
                                                <table class="" style="width:100%">
                                                    <tr>
                                                        <th style="width:37%;">
                                                            <p>Top 5</p>
                                                        </th>
                                                        <th>
                                                            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                                                <p class="">Device</p>
                                                            </div>
                                                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                                                <p class="">Progress</p>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <canvas class="canvasDoughnut" height="140" width="140" style="margin: 15px 10px 10px 0"></canvas>
                                                        </td>
                                                        <td>
                                                            <table class="tile_info">
                                                                <tr>
                                                                    <td>
                                                                        <p><i class="fa fa-square blue"></i>IOS </p>
                                                                    </td>
                                                                    <td>30%</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p><i class="fa fa-square green"></i>Android </p>
                                                                    </td>
                                                                    <td>10%</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p><i class="fa fa-square purple"></i>Blackberry </p>
                                                                    </td>
                                                                    <td>20%</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p><i class="fa fa-square aero"></i>Symbian </p>
                                                                    </td>
                                                                    <td>15%</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p><i class="fa fa-square red"></i>Others </p>
                                                                    </td>
                                                                    <td>30%</td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="x_panel tile fixed_height_320 overflow_hidden">
                                            <div class="x_title">
                                                <h2>Module A1</h2>
                                                <ul class="nav navbar-right panel_toolbox">
                                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                    </li>
                                                    <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li><a href="#">Settings 1</a>
                                                            </li>
                                                            <li><a href="#">Settings 2</a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                                    </li>
                                                </ul>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="x_content">
                                                <table class="" style="width:100%">
                                                    <tr>
                                                        <th style="width:37%;">
                                                            <p>Top 5</p>
                                                        </th>
                                                        <th>
                                                            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                                                <p class="">Device</p>
                                                            </div>
                                                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                                                <p class="">Progress</p>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <canvas class="canvasDoughnut" height="140" width="140" style="margin: 15px 10px 10px 0"></canvas>
                                                        </td>
                                                        <td>
                                                            <table class="tile_info">
                                                                <tr>
                                                                    <td>
                                                                        <p><i class="fa fa-square blue"></i>IOS </p>
                                                                    </td>
                                                                    <td>30%</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p><i class="fa fa-square green"></i>Android </p>
                                                                    </td>
                                                                    <td>10%</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p><i class="fa fa-square purple"></i>Blackberry </p>
                                                                    </td>
                                                                    <td>20%</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p><i class="fa fa-square aero"></i>Symbian </p>
                                                                    </td>
                                                                    <td>15%</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p><i class="fa fa-square red"></i>Others </p>
                                                                    </td>
                                                                    <td>30%</td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>


                                   <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="x_panel tile fixed_height_320 overflow_hidden">
                                            <div class="x_title">
                                                <h2>Module A2</h2>
                                                <ul class="nav navbar-right panel_toolbox">
                                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                    </li>
                                                    <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li><a href="#">Settings 1</a>
                                                            </li>
                                                            <li><a href="#">Settings 2</a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                                    </li>
                                                </ul>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="x_content">
                                                <table class="" style="width:100%">
                                                    <tr>
                                                        <th style="width:37%;">
                                                            <p>Top 5</p>
                                                        </th>
                                                        <th>
                                                            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                                                <p class="">Device</p>
                                                            </div>
                                                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                                                <p class="">Progress</p>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <canvas class="canvasDoughnut" height="140" width="140" style="margin: 15px 10px 10px 0"></canvas>
                                                        </td>
                                                        <td>
                                                            <table class="tile_info">
                                                                <tr>
                                                                    <td>
                                                                        <p><i class="fa fa-square blue"></i>IOS </p>
                                                                    </td>
                                                                    <td>30%</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p><i class="fa fa-square green"></i>Android </p>
                                                                    </td>
                                                                    <td>10%</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p><i class="fa fa-square purple"></i>Blackberry </p>
                                                                    </td>
                                                                    <td>20%</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p><i class="fa fa-square aero"></i>Symbian </p>
                                                                    </td>
                                                                    <td>15%</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p><i class="fa fa-square red"></i>Others </p>
                                                                    </td>
                                                                    <td>30%</td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                
                                <br>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="x_panel tile fixed_height_320 overflow_hidden">
                                            <div class="x_title">
                                                <h2>Module A3</h2>
                                                <ul class="nav navbar-right panel_toolbox">
                                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                    </li>
                                                    <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li><a href="#">Settings 1</a>
                                                            </li>
                                                            <li><a href="#">Settings 2</a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                                    </li>
                                                </ul>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="x_content">
                                                <table class="" style="width:100%">
                                                    <tr>
                                                        <th style="width:37%;">
                                                            <p>Top 5</p>
                                                        </th>
                                                        <th>
                                                            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                                                <p class="">Device</p>
                                                            </div>
                                                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                                                <p class="">Progress</p>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <canvas class="canvasDoughnut" height="140" width="140" style="margin: 15px 10px 10px 0"></canvas>
                                                        </td>
                                                        <td>
                                                            <table class="tile_info">
                                                                <tr>
                                                                    <td>
                                                                        <p><i class="fa fa-square blue"></i>IOS </p>
                                                                    </td>
                                                                    <td>30%</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p><i class="fa fa-square green"></i>Android </p>
                                                                    </td>
                                                                    <td>10%</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p><i class="fa fa-square purple"></i>Blackberry </p>
                                                                    </td>
                                                                    <td>20%</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p><i class="fa fa-square aero"></i>Symbian </p>
                                                                    </td>
                                                                    <td>15%</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p><i class="fa fa-square red"></i>Others </p>
                                                                    </td>
                                                                    <td>30%</td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="x_panel tile fixed_height_320 overflow_hidden">
                                            <div class="x_title">
                                                <h2>Module A4</h2>
                                                <ul class="nav navbar-right panel_toolbox">
                                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                    </li>
                                                    <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li><a href="#">Settings 1</a>
                                                            </li>
                                                            <li><a href="#">Settings 2</a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                                    </li>
                                                </ul>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="x_content">
                                                <table class="" style="width:100%">
                                                    <tr>
                                                        <th style="width:37%;">
                                                            <p>Top 5</p>
                                                        </th>
                                                        <th>
                                                            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                                                <p class="">Device</p>
                                                            </div>
                                                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                                                <p class="">Progress</p>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <canvas class="canvasDoughnut" height="140" width="140" style="margin: 15px 10px 10px 0"></canvas>
                                                        </td>
                                                        <td>
                                                            <table class="tile_info">
                                                                <tr>
                                                                    <td>
                                                                        <p><i class="fa fa-square blue"></i>IOS </p>
                                                                    </td>
                                                                    <td>30%</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p><i class="fa fa-square green"></i>Android </p>
                                                                    </td>
                                                                    <td>10%</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p><i class="fa fa-square purple"></i>Blackberry </p>
                                                                    </td>
                                                                    <td>20%</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p><i class="fa fa-square aero"></i>Symbian </p>
                                                                    </td>
                                                                    <td>15%</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p><i class="fa fa-square red"></i>Others </p>
                                                                    </td>
                                                                    <td>30%</td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="x_panel tile fixed_height_320 overflow_hidden">
                                            <div class="x_title">
                                                <h2>Module A5</h2>
                                                <ul class="nav navbar-right panel_toolbox">
                                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                    </li>
                                                    <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li><a href="#">Settings 1</a>
                                                            </li>
                                                            <li><a href="#">Settings 2</a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                                    </li>
                                                </ul>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="x_content">
                                                <table class="" style="width:100%">
                                                    <tr>
                                                        <th style="width:37%;">
                                                            <p>Top 5</p>
                                                        </th>
                                                        <th>
                                                            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                                                <p class="">Device</p>
                                                            </div>
                                                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                                                <p class="">Progress</p>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <canvas class="canvasDoughnut" height="140" width="140" style="margin: 15px 10px 10px 0"></canvas>
                                                        </td>
                                                        <td>
                                                            <table class="tile_info">
                                                                <tr>
                                                                    <td>
                                                                        <p><i class="fa fa-square blue"></i>IOS </p>
                                                                    </td>
                                                                    <td>30%</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p><i class="fa fa-square green"></i>Android </p>
                                                                    </td>
                                                                    <td>10%</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p><i class="fa fa-square purple"></i>Blackberry </p>
                                                                    </td>
                                                                    <td>20%</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p><i class="fa fa-square aero"></i>Symbian </p>
                                                                    </td>
                                                                    <td>15%</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p><i class="fa fa-square red"></i>Others </p>
                                                                    </td>
                                                                    <td>30%</td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                
                                

                                </div>
                                <center>
<input type=button onclick=window.location.href='index.php?gestion=situation&action=ficheSituation'; value="Ajouter une situation" />
<input type=button onclick=window.location.href=''; value="Ajouter un commentaire" />
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /page content -->
            
            <!-- footer content -->
            <footer>
                <div class="pull-right">
                    Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
                </div>
                <div class="clearfix"></div>
            </footer>
            <!-- /footer content -->
        </div>
    </div>

    <!-- jQuery -->
    <script src="template/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="template/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="template/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="template/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="template/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="template/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="template/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="template/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="template/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="template/vendors/Flot/jquery.flot.js"></script>
    <script src="template/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="template/vendors/Flot/jquery.flot.time.js"></script>
    <script src="template/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="template/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="template/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="template/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="template/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="template/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="template/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="template/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="template/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="template/vendors/moment/min/moment.min.js"></script>
    <script src="template/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="template/build/js/custom.min.js"></script>

</body>
</html>
